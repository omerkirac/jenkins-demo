package com.first.demo.entity;

import lombok.Data;

@Data
public class HelloEntity {
    private String name;
}
