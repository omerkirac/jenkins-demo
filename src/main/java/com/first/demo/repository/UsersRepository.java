package com.first.demo.repository;

import com.first.demo.entity.UsersEntity;
import org.apache.catalina.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UsersRepository extends JpaRepository<UsersEntity, Long> {
    UsersEntity findByUserId(Long id);

    List<UsersEntity> findAllByPwd(String pass);
}
