package com.first.demo.sevice;
import com.first.demo.entity.UsersEntity;

import java.util.List;

public interface IHelloService {
    UsersEntity getUser(Long id);

    List<UsersEntity> getAllUsers();
}
