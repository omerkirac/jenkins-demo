package com.first.demo.sevice;

import com.first.demo.entity.UsersEntity;
import com.first.demo.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HelloServiceImpl implements IHelloService {
    private final UsersRepository usersRepository;

    @Autowired
    public HelloServiceImpl(UsersRepository usersRepository) {
        this.usersRepository = usersRepository;
    }

    @Override
    public UsersEntity getUser(Long id) {
        return usersRepository.findByUserId(id);
    }

    @Override
    public List<UsersEntity> getAllUsers() {
        return usersRepository.findAllByPwd("12345");
    }
}
