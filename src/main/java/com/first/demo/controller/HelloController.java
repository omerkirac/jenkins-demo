package com.first.demo.controller;

import com.first.demo.entity.UsersEntity;
import com.first.demo.sevice.IHelloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/users")
public class HelloController {

    private final IHelloService helloService;

    @Autowired
    public HelloController(IHelloService helloService) {
        this.helloService = helloService;
    }

    @GetMapping
    public ResponseEntity<List<UsersEntity>> inquireUsers(){
        List<UsersEntity> users = helloService.getAllUsers();
        return ResponseEntity.ok(users);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<UsersEntity> inquireUser(@PathVariable(value = "id") Long id){
        UsersEntity user = helloService.getUser(id);
        return ResponseEntity.ok(user);
    }
}
